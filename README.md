# androiden

### These are the androiden you are looking for.

##### Containers for building android applications

  androiden-base-[8|11|17|18]
  : focal based containers with temurin and android sdk(manager)

  androiden-rust-[8|11|17|18]
  : base + rust

  androiden-go-[8|11|17|18]
  : base + go 1.18 + gomobile

##### Containers for building containers for building android applications

  androiden-archive
  : sdk|ndk|gradle archives, so the android.com and gradle download servers are not hurt ;)

##### Containers for building android itself

  not yet
  : not yet
